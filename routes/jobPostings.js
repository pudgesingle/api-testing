const {
  searchJobPostingWithText, getJobPostingWithId,
  applyJobPosting
} = require('../controllers/jobPostings')
const express = require('express')
const router = express.Router()

router.get('/search/:text', searchJobPostingWithText)

router.get('/:id', getJobPostingWithId)

router.post('/:id/apply', applyJobPosting)
module.exports = router
